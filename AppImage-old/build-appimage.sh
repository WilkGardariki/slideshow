#!/bin/bash

PATH=~/.local/bin:~/Qt5.10.0/5.10.0/gcc_64/bin:$PATH

make clean

linuxdeployqt OphtalmologicExamination \
	-appimage \
	-no-copy-copyright-files

# Примерно так должно быть
#linuxdeployqt ./usr/share/applications/OphtalmologicExamination.desktop \
#	-appimage \
#	-no-copy-copyright-files
