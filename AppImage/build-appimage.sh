#!/bin/bash

PATH=~/.local/bin:~/Qt5.10.0/5.10.0/gcc_64/bin:$PATH

# Примерно так должно быть
linuxdeployqt OphtalmologicExamination.desktop \
	-appimage \
	-no-copy-copyright-files
