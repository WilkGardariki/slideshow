#####################
# Find dependencies #
#####################
function (OphtalmologicExamination_find_dependencies)
  set (_options
    USE_FIND_DEPENDENCIES
    )
  set (_multi_value_args
    ""
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})


  if (i_USE_FIND_DEPENDENCIES)
    macro (find_function)
      include (CMakeFindDependencyMacro)

      find_dependency (${ARGN})
    endmacro (find_function)
  else ()
    macro (find_function)
      find_package (${ARGN})
    endmacro (find_function)
  endif ()

  find_function (Qt5 5.4 REQUIRED
    COMPONENTS
    Gui
    Sql
    Widgets
    )
endfunction (OphtalmologicExamination_find_dependencies)


####################
# Use dependencies #
####################
function (OphtalmologicExamination_use_dependencies)
  set (_options
    ""
    )
  set (_multi_value_args
    TARGET
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})

  if (NOT i_TARGET)
    message (SEND_ERROR "Provide TARGET argument")
  endif ()

  foreach (_target IN LISTS i_TARGET)
    target_link_libraries (
      ${_target}
      PUBLIC
      Qt5::Gui
      Qt5::Sql
      Qt5::Widgets
      )
  endforeach (_target IN LISTS _interface_targets)
endfunction (OphtalmologicExamination_use_dependencies)
