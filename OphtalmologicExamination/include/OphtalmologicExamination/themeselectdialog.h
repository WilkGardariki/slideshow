#ifndef THEMESELECTDIALOG_H
#define THEMESELECTDIALOG_H

#include <QApplication>
#include <QBoxLayout>
#include <QDesktopWidget>
#include <QDialog>
#include <QEvent>
#include <QKeyEvent>
#include <QMessageBox>
#include <QPushButton>
#include <QXmlStreamReader>

#include "examdialog.h"
#include "modedialog.h"
#include "reportdialog.h"
#include "structures.h"
#include "trainingdialog.h"


class ModeDialog;


class ThemeSelectDialog : public QDialog {
	Q_OBJECT

public:
	explicit ThemeSelectDialog (
	    QStringList themes, int mode, ModeDialog* parent = 0);
	~ThemeSelectDialog ();


	QVector< Slide > topSlides;
	QVector< Slide > bottomSlides;

	bool themeParse (QString dir);
	Slide slideParse ();


	ModeDialog* getParent () const;

	QString getSelectedTheme () const;

	int getExamNumberOfQuestion () const;

	void keyPressEvent (QKeyEvent* event);


private slots:
	void toModeDialog ();
	void sendSelectedTheme ();

private:
	void CreateDummyDialogAndConnectToModeDialog (QDialog* i_mode_dialog);


	ModeDialog* parent;
	int mode;
	QString selectedTheme;
	int examNumberOfQuestion;

	QXmlStreamReader Rxml;

	void reject ();
};

#endif // THEMESELECTDIALOG_H
