#include "OphtalmologicExamination/dummydialog.h"
#include "OphtalmologicExamination/ui_dummydialog.h"


#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QSettings>


DummyDialog::DummyDialog (
    QVector< Slide > *topSlides,
    QVector< Slide > *bottomSlides,
    ThemeSelectDialog *parent)
    : QDialog (parent)
    , ui (new Ui::DummyDialog) {
	ui->setupUi (this);

	LoadConfig ();

	this->parent = parent;
	this->topSlides = topSlides;
	this->bottomSlides = bottomSlides;


	this->setWindowModality (Qt::NonModal);
	this->setWindowFlags (Qt::ToolTip | Qt::FramelessWindowHint);

	currentTopSlide = 0;
	topTime = QTime::currentTime ();
	currentBottomSlide = 0;
	bottomTime = QTime::currentTime ();

	if (!topSlides->isEmpty ())
		setTopSlide (currentTopSlide);
	if (!bottomSlides->isEmpty ())
		setBottomSlide (currentBottomSlide);
}

DummyDialog::~DummyDialog () {
	delete ui;
}


void DummyDialog::LoadConfig () {
    auto const config_file_name =
        QDir (QDir::currentPath ())
            .absoluteFilePath ("config/DummyDialog.ini");

	if (!QFile::exists (config_file_name)) {
		auto error_dialog =
		    new QMessageBox{QMessageBox::Critical,
		                    tr ("Ошибка"),
		                    tr ("Отсутствует файл настроек окна куклы"),
		                    QMessageBox::Ok,
		                    this};

		error_dialog->setAttribute (Qt::WA_DeleteOnClose);
		error_dialog->show ();

		return;
	}

	QSettings settings{config_file_name, QSettings::IniFormat};

	settings.beginGroup ("DummyDialog");
	{
		auto size_variant = settings.value ("size", QVariant{});
		auto top_slide_pos_variant =
		    settings.value ("top_slide_pos", QVariant{});
		auto bottom_slide_pos_variant =
		    settings.value ("bottom_slide_pos", QVariant{});

		if (!(size_variant.isValid () && top_slide_pos_variant.isValid () &&
		      bottom_slide_pos_variant.isValid ())) {
			auto error_dialog =
			    new QMessageBox{QMessageBox::Critical,
			                    tr ("Ошибка"),
			                    tr ("Некорректный файл настроек"),
			                    QMessageBox::Ok,
			                    this};

			error_dialog->setAttribute (Qt::WA_DeleteOnClose);
			error_dialog->show ();

			return;
		}

		if (!(size_variant.canConvert< QSize > () &&
		      top_slide_pos_variant.canConvert< QPoint > () &&
		      bottom_slide_pos_variant.canConvert< QPoint > ())) {
			auto error_dialog =
			    new QMessageBox{QMessageBox::Critical,
			                    tr ("Ошибка"),
			                    tr ("Некорректный файл настроек"),
			                    QMessageBox::Ok,
			                    this};

			error_dialog->setAttribute (Qt::WA_DeleteOnClose);
			error_dialog->show ();

			return;
		}

		ui->topEye->resize (size_variant.toSize ());
		ui->topEye->move (top_slide_pos_variant.toPoint ());

		ui->bottomEye->resize (size_variant.toSize ());
		ui->bottomEye->move (bottom_slide_pos_variant.toPoint ());
	}
	settings.endGroup ();
}


void DummyDialog::setTopSlide (int i) {
	QImage img (topSlides->at (i).imagePath);
	QImage scaledImage = img.scaled (
	    ui->topEye->size (), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	ui->topEye->setPixmap (QPixmap::fromImage (scaledImage));
}

void DummyDialog::setBottomSlide (int i) {
	QImage img (bottomSlides->at (i).imagePath);
	QImage scaledImage = img.scaled (
	    ui->bottomEye->size (),
	    Qt::IgnoreAspectRatio,
	    Qt::SmoothTransformation);
	ui->bottomEye->setPixmap (QPixmap::fromImage (scaledImage));
}

void DummyDialog::reject () {
	done (Rejected);
}
