#include "OphtalmologicExamination/logindialog.h"
#include <QApplication>

#include "OphtalmologicExamination/RunGuard.h"

int main (int argc, char *argv[]) {    
    RunGuard guard( "some_random_key" );
    if ( !guard.tryToRun() )
        return 0;

	QApplication a (argc, argv);
	LoginDialog d;
    d.show ();

	return a.exec ();
}
