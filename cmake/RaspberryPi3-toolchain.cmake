set (CMAKE_SYSTEM_NAME Linux)
set (CMAKE_SYSTEM_PROCESSOR arm)

set (CMAKE_SYSROOT "" CACHE PATH "Path to Raspberry Pi 3 sysroot")
set (CMAKE_STAGING_PREFIX  "" CACHE PATH "Path to Raspberry Pi 3 staging area")

set (RASPBERRY_PI_3_TOOLS_ROOT "" CACHE PATH "Path to Raspberry Pi 3 toolchain root directory")

set (CMAKE_C_COMPILER    ${RASPBERRY_PI_3_TOOLS_ROOT}/bin/arm-linux-gnueabihf-gcc)
set (CMAKE_CXX_COMPILER  ${RASPBERRY_PI_3_TOOLS_ROOT}/bin/arm-linux-gnueabihf-g++)

set (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set (CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set (_lib_path "${CMAKE_SYSROOT}/lib/arm-linux-gnueabihf")
set (_opt_lib_path "${CMAKE_SYSROOT}/opt/vc/lib")
set (_usr_lib_path "${CMAKE_SYSROOT}/usr/lib/arm-linux-gnueabihf")

set (CMAKE_EXE_LINKER_FLAGS
  "-Wl,-rpath-link,${_lib_path} -Wl,-rpath-link,${_opt_lib_path} -Wl,-rpath-link,${_usr_lib_path}"
  CACHE STRING "rpath-link arguments are required to compile executables"
  )
