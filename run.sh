# !/usr/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/pi/oph/Qt5/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/pi/oph/cutelyst/usr/local/lib/cutelyst2-plugins
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/pi/oph/cutelyst/usr/local/lib/arm-linux-gnueabihf
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/pi/oph/grantlee/lib

export CUTELYST_PLUGINS_DIR=/home/pi/oph/cutelyst/usr/local/lib/cutelyst2-plugins

export QT_PLUGIN_DIR=/home/pi/oph/grantlee/lib:$CUTELYST_PLUGINS_DIR
# export QT_PLUGINS_DIR=/home/pi/oph/grantlee/lib

# export GRANTLEE_PLUGIN_PATH=$QT_PLUGIN_DIR
# export GRANTLEE_PLUGINS_PATH=$QT_PLUGIN_DIR

# export GRANTLEE_PLUGIN_DIR=$QT_PLUGIN_DIR
# export GRANTLEE_PLUGINS_DIR=$QT_PLUGIN_DIR

export QT_DIR=/home/pi/oph/Qt5
export QT_PLUGIN_PATH=/home/pi/oph/Qt5/plugins:$QT_PLUGIN_DIR
export QT_QPA_PLATFORM_PLUGIN_PATH=/home/pi/oph/Qt5/plugins/platforms

./OphtalmologicExamination-armv6-0_3_0-0.3.0
